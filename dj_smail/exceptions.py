class SecretKeyHasChangedError(Exception):
    """The secret key of the application has changed"""
    pass
