=======
CHANGES
=======

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

0.x.0 (unreleased)
------------------

-

0.7.0 (2021-03-16)
------------------

- change: README in markdown

0.2.0 (2021-03-14)
------------------

- add serial_number
- rename to dj_smail

0.1.0 (2020-03-14)
------------------

- Initial release.
